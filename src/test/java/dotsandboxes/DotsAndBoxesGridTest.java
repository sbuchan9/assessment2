package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }
    
    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testBoxCompleteWorking() {
        logger.info("Test that box completion is true for a valid box coordinate");
        
        // First draw a valid box
        DotsAndBoxesGrid dotsAndBoxesGrid = new DotsAndBoxesGrid(8, 8, 2);
        dotsAndBoxesGrid.drawHorizontal(1, 1, 1);
        dotsAndBoxesGrid.drawHorizontal(1, 2, 1);
        dotsAndBoxesGrid.drawVertical(1, 1, 1);
        dotsAndBoxesGrid.drawVertical(2, 1, 1);
        // Assert our box is valid
        boolean isBox = dotsAndBoxesGrid.boxComplete(1, 1);
        assertTrue(isBox);

        // Now we will test with an invalid box, which should fail
        logger.info("Test that box completion is false for an invalid box coordinate");
        
        // First draw only a partial box
        dotsAndBoxesGrid = new DotsAndBoxesGrid(8, 8, 2);
        dotsAndBoxesGrid.drawHorizontal(1, 1, 1);
        dotsAndBoxesGrid.drawHorizontal(1, 2, 1);
        dotsAndBoxesGrid.drawVertical(1, 1, 1);
        // Now assert an unfinished box isn't valid
        isBox = dotsAndBoxesGrid.boxComplete(1, 1);
        assertFalse(isBox);

    }
        
    @Test
    public void testDoubleDrawForbidden() {
        logger.info("Test that drawing the same line twice is forbidden (horizontal)");
        // Instantiate the grid and draw a horizontal line twice, assert an exception
        DotsAndBoxesGrid dotsAndBoxesGrid = new DotsAndBoxesGrid(8, 8, 2);
        dotsAndBoxesGrid.drawHorizontal(1, 1, 1);
        assertThrows(Exception.class, () -> dotsAndBoxesGrid.drawHorizontal(1, 1, 1));

        logger.info("Test that drawing the same line twice is forbidden (vertical)");
        // Instantiate the grid and draw a vertical line twice, assert an exception
        DotsAndBoxesGrid dotsAndBoxesGrid2 = new DotsAndBoxesGrid(8, 8, 2);
        dotsAndBoxesGrid2.drawVertical(1, 1, 1);
        assertThrows(Exception.class, () -> dotsAndBoxesGrid2.drawVertical(1, 1, 1));
    }
    
}
